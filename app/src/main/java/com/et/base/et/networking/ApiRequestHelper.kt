package com.et.base.et.networking

import com.et.base.et.enums.ApiAction
import com.et.base.et.helper.LogHelper
import com.et.base.et.singleton.RetrofitSingleton
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class ApiRequestHelper {

    private var onApiRequestListener: ApiRequestListener? = null
    private var apiInterface: ApiInterface? = null

    fun ApiRequestHelper(onApiRequestListener: ApiRequestListener){
        this.onApiRequestListener = onApiRequestListener
        this.apiInterface = RetrofitSingleton.instance.apiInterface

    }

    /**
     * handle api result using lambda
     *
     * @param action     identification of the current api request
     * @param observable actual process of the api request
     */
    private fun handleObservableResult(action: ApiAction, observable: Observable<*>) {
        observable.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe({ result -> onApiRequestListener?.onApiRequestSuccess(action, result) },
                        { throwable -> onApiRequestListener?.onApiRequestFailed(action, throwable as Throwable) },
                        { LogHelper.log("api", "api request completed --> $action") })
    }

    /****************
     * API METHOD CALLS
     */
    fun login() {
        onApiRequestListener?.onApiRequestStart(ApiAction.LOGIN)
        val observable = apiInterface?.getLocations("change to shared pref","application/json")
        handleObservableResult(ApiAction.LOGIN, observable!!)
    }
}