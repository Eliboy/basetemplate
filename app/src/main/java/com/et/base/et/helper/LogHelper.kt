package com.et.base.et.helper

import android.util.Log
import com.et.base.et.BuildConfig

object LogHelper {
    fun log(key: String, message: String) {
                if (BuildConfig.DEBUG) {
                    Log.d(key,message)
                }
        //remove when app is for releasing
        Log.e(key, message)
    }
}