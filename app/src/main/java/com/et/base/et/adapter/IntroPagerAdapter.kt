package com.et.base.et.adapter

import android.graphics.Color
import android.widget.TextView
import android.view.ViewGroup
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import com.et.base.et.R
import com.et.base.et.model.IntroData
import java.util.*
import android.view.animation.AnimationUtils
import android.view.animation.Animation



// 1
class IntroPagerAdapter(private var introDataList: ArrayList<IntroData>) : PagerAdapter() {



    override fun isViewFromObject(v: View, `object`: Any): Boolean {
        // Return the current view
        return v === `object` as View
    }


    override fun getCount(): Int {
        // Count the items and return it
        return introDataList.size
    }


    override fun instantiateItem(parent: ViewGroup, position: Int): Any {
        // Get the view from pager page layout
        val view = LayoutInflater.from(parent?.context)
                .inflate(R.layout.content_intro,parent,false)

        // Get the widgets reference from layout
        val linearLayout: LinearLayout = view.findViewById(R.id.linear_layout)
        val tvLower: TextView = view.findViewById(R.id.tv_lower)
        val tvLUpper: TextView = view.findViewById(R.id.tv_upper)
        val tvFooter: TextView = view.findViewById(R.id.tv_footer)


        // Set the text views text
        tvLower.text = introDataList.get(position).title
        tvLUpper.text = introDataList.get(position).description
        tvFooter.text = "Page ${position+1} of ${introDataList.size}"

        // Initially hide the content view.
//        tvLower.visibility = View.GONE
//        tvLUpper.visibility = View.GONE

        val animFadeIn = AnimationUtils.loadAnimation(view.context, R.anim.fade_in)
        tvLower.startAnimation(animFadeIn)



        // Set the text views text color
//        tvLower.setTextColor(randomLightColor(70,80))
//        tvLUpper.setTextColor(randomLightColor(90,70))

        // Set the layout background color
        linearLayout.setBackgroundColor(randomLightColor(10))

        // Add the view to the parent
        parent?.addView(view)

        // Return the view
        return view
    }


    override fun destroyItem(parent: ViewGroup, position: Int, `object`: Any) {
        // Remove the view from view group specified position
        parent.removeView(`object` as View)
    }



    // Generate random light hsv color
    private fun randomLightColor(lightPercent:Int,blackPercent:Int=100):Int{
        // Color variance - red, green, blue etc
        val hue = Random().nextInt(361).toFloat()

        // Color light to dark - 0 light 100 dark
        val saturation = lightPercent.toFloat()/100F

        // Color black to bright - 0 black 100 bright
        val brightness:Float = blackPercent.toFloat()/100F

        // Transparency level, full opaque
        val alpha = 255

        // Return the color
        return Color.HSVToColor(alpha,floatArrayOf(hue,saturation,brightness))
    }


}