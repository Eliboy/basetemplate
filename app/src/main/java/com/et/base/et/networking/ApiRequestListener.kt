package com.et.base.et.networking

import com.et.base.et.enums.ApiAction
import java.util.*

/**
 * Created by PMTI-32 on 10/02/2017.
 */

interface ApiRequestListener {

    /**
     * triggered when an api request starts
     *
     * @param apiAction identification of which api request is starting
     */
    fun onApiRequestStart(apiAction: ApiAction)

    /**
     * triggered when an api request fails
     *
     * @param apiAction identification of which api request fails
     * @param t         contains the cause of api request failure
     */
    fun onApiRequestFailed(apiAction: ApiAction, t: Throwable)

    /**
     * triggered when an api request was successfully finished
     *
     * @param apiAction identification of which api request succeed
     * @param result    contains the result/response of api
     */
    fun onApiRequestSuccess(apiAction: ApiAction, result: Any)
}
