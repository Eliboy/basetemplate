package com.et.base.et.base

import android.content.Context
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
import com.et.base.et.BuildConfig
import com.et.base.et.R
import com.et.base.et.singleton.RetrofitSingleton
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import java.util.concurrent.TimeUnit

/**
 * Created by Eli Tamondong on 25/09/2018.
 */

class BaseApplication : MultiDexApplication() {

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()

        /** initialize okhttp client  */
        var cacheDir = externalCacheDir
        if (cacheDir == null) {
            cacheDir = getCacheDir()
        }

        val cache = Cache(cacheDir, (10 * 1024 * 1024).toLong())


        //        Raven.init(this, DNS_DEBUG);


        val logging = HttpLoggingInterceptor()

        logging.level = HttpLoggingInterceptor.Level.BODY

        /** initialize ok http client  */

        val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(logging)
                .cache(cache)
                .connectTimeout(CONNECTION_TIME_OUT.toLong(), TimeUnit.SECONDS)
                .readTimeout(READ_TIME_OUT.toLong(), TimeUnit.SECONDS)
                .build()


        /** initialize dao  */
//        DaoSingleton.getInstance().initDao(this)

        /** initialize retrofit  */
        val retrofitSingleton = RetrofitSingleton.instance
        retrofitSingleton.initRetrofit(BuildConfig.BASE_URL, okHttpClient)


        /** initialize encryption util  */
//        val encryptionSingleton = EncryptionSingleton.getInstance()
//        encryptionSingleton.initEncryption(true)

        /** initialize shared preference  */
//        val sharedPrefSingleton = SharedPrefSingleton.getInstance()
//        sharedPrefSingleton.initSharedPref(this)

        //  /*/
        //        This sends a simple event to Sentry using the statically stored instance
        //        that was created in the ``main`` method.
        //        */
        //        Raven.capture("This is a test");

        /** initialize calligraphy  */
        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Roboto-ThinItalic.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build())
    }

    companion object {

        private val CONNECTION_TIME_OUT = 70
        private val READ_TIME_OUT = 70

    }
}