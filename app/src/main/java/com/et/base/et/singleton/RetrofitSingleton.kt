package com.et.base.et.singleton

import com.et.base.et.networking.ApiInterface
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitSingleton private constructor() {
    var retrofit: Retrofit? = null
        private set
    var apiInterface: ApiInterface? = null
        private set

    fun initRetrofit(baseUrl: String, okHttpClient: OkHttpClient) {

        val GSON = GsonBuilder()
                .serializeNulls()
                .create()

        retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(GSON))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build()

        apiInterface = retrofit!!.create(ApiInterface::class.java)
    }

    companion object {
        val instance = RetrofitSingleton()
    }



}
