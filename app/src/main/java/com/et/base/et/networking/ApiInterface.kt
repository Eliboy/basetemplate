package com.et.base.et.networking

import retrofit2.http.Header
import retrofit2.http.POST
import rx.Observable
import java.util.ArrayList

interface ApiInterface {

    @POST("pbr/ws/rs/pbrsearch/RetrievePbrSearchParameters")
    fun getLocations(@Header("Authorization") aut: String, @Header("Content-Type") contentType: String): Observable<ArrayList<String>>

}