package com.et.base.et.view

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity


import android.support.v4.view.ViewPager
import android.widget.ImageView
import android.widget.LinearLayout
import com.et.base.et.R
import com.et.base.et.adapter.IntroPagerAdapter
import com.et.base.et.model.IntroData
import kotlinx.android.synthetic.main.activity_intro.*


class IntroCoffee : AppCompatActivity() {

    private lateinit var viewPager: ViewPager
    var introDataList = ArrayList<IntroData>()
    private var dots: List<ImageView>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intro)


        dots = java.util.ArrayList()
        val dotsLayout: LinearLayout = findViewById(R.id.dots)
        // Initialize a list of string values
        val alphabets = listOf("a","b","c","d","e","f")


        for ((index, value) in alphabets.withIndex()) {
            var introData1 = IntroData(index,"Discover- $value","Brew your way, discover and have a sip of our new flavors!",index)

            introDataList.add(introData1)
        }

        // Initialize a new pager adapter instance with list
        val adapter = IntroPagerAdapter(introDataList)

        // Finally, data bind the view pager widget with pager adapter
        view_pager.adapter = adapter


        for (i in 0 until introDataList.size) {
            val dot = ImageView(this)
            dot.setImageResource(R.drawable.ic_unselected_dot)

            val params = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            )
            dotsLayout.addView(dot, params)

            (dots as java.util.ArrayList<ImageView>).add(dot)
        }

        // set page 1
        selectDot(0)
        view_pager.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

            override fun onPageSelected(position: Int) {
                selectDot(position)

            }

            override fun onPageScrollStateChanged(state: Int) {}
        })

        skip.setOnClickListener{
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            this.finish()
        }
    }


    fun selectDot(idx: Int) {
        val res = resources
        for (i in 0 until introDataList.size) {
            val drawableId = if (i == idx) R.drawable.ic_selected_dot else R.drawable.ic_unselected_dot
            val drawable = res.getDrawable(drawableId)
            dots?.get(i)?.setImageDrawable(drawable)
        }
    }


}
