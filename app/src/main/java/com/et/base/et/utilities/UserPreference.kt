package com.et.base.et.utilities

import android.content.Context
import android.content.SharedPreferences
import com.et.base.et.BuildConfig

class UserPreference {

    private lateinit var pref: SharedPreferences
    private lateinit var editor: SharedPreferences.Editor


    // Context
    private var _context: Context? = null

    // Shared pref mode
    var PRIVATE_MODE : Int = 0



    companion object {
        const val KEY_AUTH_TOKEN = "AUTHToken"
        const val KEY_USER_NAME = "username"
        const val KEY_USER_PASS = "password"
        const val KEY_USER_EMAIL = "email"
        const val K_CONTENT_TYPE = "application/json"
    }


    fun UserPreference(context: Context){
        this._context = context
        pref = _context!!.getSharedPreferences(BuildConfig.PACKAGE_NAME, PRIVATE_MODE)
        editor = pref.edit()
    }

    fun getAUTHToken(): String? {
        return pref.getString(KEY_AUTH_TOKEN, null)
    }

    fun setAUTHToken(token: String) {
        editor.putString(KEY_AUTH_TOKEN, token)
        editor.commit()
    }

    fun setKeyUserName(username: String) {
        editor.putString(KEY_USER_NAME, username)
        editor.commit()
    }

    fun getKeyUserName(): String? {
        return pref.getString(KEY_USER_NAME, null)
    }

    fun setKeyUserPass(password: String) {
        editor.putString(KEY_USER_PASS, password)
        editor.commit()
    }

    fun getKeyUserPass(): String? {
        return pref.getString(KEY_USER_PASS, null)
    }


    fun setKeyUserEmail(email: String) {
        editor.putString(KEY_USER_EMAIL, email)
        editor.commit()
    }


    fun getKeyUserEmail(): String? {
        return pref.getString(KEY_USER_EMAIL, null)
    }

    fun getK_CONTENT_TYPE(): String {
        return K_CONTENT_TYPE
    }



}