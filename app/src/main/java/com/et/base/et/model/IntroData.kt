package com.et.base.et.model

import com.google.gson.annotations.SerializedName


data class IntroData(@SerializedName("id") var id: Int,
                     @SerializedName("title") var title: String,
                     @SerializedName("description") var description: String,
                     @SerializedName("viewCount") var viewCount: Int)
