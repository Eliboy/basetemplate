package com.et.base.et.view


import android.app.Fragment
import android.os.Bundle
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View


class FirstIntroFragment : Fragment() {

    private val ARG_LAYOUT_RES_ID = "layoutResId"
    private var layoutResId: Int = 0

    fun newInstance(layoutResId: Int): FirstIntroFragment {
        val sampleSlide = FirstIntroFragment()

        val args = Bundle()
        args.putInt(ARG_LAYOUT_RES_ID, layoutResId)
        sampleSlide.setArguments(args)

        return sampleSlide
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments != null && arguments.containsKey(ARG_LAYOUT_RES_ID)) {
            layoutResId = arguments.getInt(ARG_LAYOUT_RES_ID)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutResId, container, false)
    }
}